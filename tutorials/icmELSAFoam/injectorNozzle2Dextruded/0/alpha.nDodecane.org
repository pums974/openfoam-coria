/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.0.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volScalarField;
    location    "0";
    object      alpha.nDodecane;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 0 0 0 0 0 0];

internalField   uniform 0;


boundaryField
{

    inlet
    {
        type            fixedValue;
        value           uniform 1;
    }

    //".*Wall"
    chamberWall
    {
        type            zeroGradient;
    }
    
    chamberAtmoshpere
    {
        type            zeroGradient;
    }
    
    outlet
    {
        type            zeroGradient;
    }
    
    front
    {
        type            wedge;
    }
    
    back
    {
        type            wedge;
    }
    
    bottom           
    {
        type            zeroGradient;         
    }

}


// ************************************************************************* //
