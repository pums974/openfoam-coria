set xlabel "Radial distance from injector axis [m]"
set ylabel "Velocity Magnitude [m/s]"
set title "Umag along a radial line 1.5 mm from Injector axis, time 2.6e-5 s" 
plot "postProcessing/singleGraph/2.6e-05/line_U.xy" u 1:(($2)**2 +($3)**2 +($4)**2)**0.5 w l lw 3 title "elsaBaseFoam 5.0"

set grid
set terminal push
set terminal eps
set output "Umag.eps"
replot
set terminal png
set output "Umag.png"
replot
set terminal pop
