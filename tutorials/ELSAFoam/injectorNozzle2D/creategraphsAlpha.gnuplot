set style line 1 lc rgb 'black' pt 5   # square
set style line 2 lc rgb 'black' pt 7   # circle
set style line 3 lc rgb 'black' pt 9   # triangle

set xlabel "Radial distance from injector axis [m]"
set ylabel "alpha [-]"
set title "alpha along a radial line 1.5 mm from Injector axis, time 2.6e-5 s" 
plot "postProcessing/singleGraph/2.6e-05/line_p_rgh_elsaSD32_elsaSigmaPrime_alpha.nDodecane.xy" u 1:5 w l lw 3 title "elsaBaseFoam 5.0"
set grid
set terminal push
set terminal eps
set output "alpha.eps"
replot
set terminal png
set output "alpha.png"
replot
set terminal pop
