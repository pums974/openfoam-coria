/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  4.x                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

convertToMeters 1e-3;

nozR 44.7e-3;
boxLength 10.0;
boxDia 3.5;

// Mesh input parameters
nyNoz 10;  //number of cells aling nozzle radius
RxBox 50.0; // max/min cell size ratio along x dierection
RyBox 50.0;

cellSizeNozY #calc "($nozR/$nyNoz)";
minCellSizeBoxX $cellSizeNozY;
maxCellSizeBoxX #calc "($minCellSizeBoxX*$RxBox)";

//progression in box x-direction (first to second cell size ratio)
rxBox  #calc "($boxLength-$minCellSizeBoxX)/($boxLength-$minCellSizeBoxX*$RxBox)";
nxBox #calc "(floor(log($RxBox)/log($rxBox)+1))";
//R=2;
// progession ratio
minCellSizeBoxY $cellSizeNozY;
maxCellSizeBoxY #calc "($minCellSizeBoxY*$RyBox)";

ryBox  #calc "($boxDia-$nozR-$minCellSizeBoxY)/($boxDia-$nozR-$minCellSizeBoxY*$RyBox)";
nyBox #calc "(floor(log($RyBox)/log($ryBox)+1))";

//progressionBoxDia #calc "($boxDia-$nozSmallest)/($boxDia-$nozSmallest*$RyBox)";
//ryBox $progressionBoxDia; 
//nyBox 1;
#codeStream
     {
          codeInclude
           #{
              #include "pointField.H" 
              #include <iostream>
              #include <fstream>
              #include <cmath>
           #};
               code
               #{
                  Info  << "ryBox" << endl;
                  //Info  << $ryBox << endl;
                  //nxBox 0;
                 // nyBox #calc "(floor(($RyBox)/($ryBox)+1))" ;
                  //nxBox #calc "(floor(log($RxBox)/log($rxBox)+1))";
                  Info  << "nxBox" << endl;
                  Info  << $nxBox << endl;         
               #};
       };


vertices
(
        (0.00  0.00 -0.5) // 0, origin
       ($boxLength 0.00 -0.5) // 1
       ($boxLength $nozR -0.5) // 2
       (0.00 $nozR -0.5) // 3, 0-1-2-3 form the back plane in x-y
       (0.00 $boxDia -0.5) // 4, 
       ($boxLength $boxDia -0.5) // 5
        
        //front side
        (0.00 0.00 0.00) // 6, front side of 0
        ($boxLength 0.00 0.00) // 7, front side of 1
        ($boxLength $nozR 0.00) // 8, front side of 2
        (0.00 $nozR 0.00) // 9, front side of 3
        (0.00 $boxDia 0.00) // 10, front side of 4
        ($boxLength $boxDia 0.00) // 11 , front side of 5
);

blocks
(
   hex (0 1 2 3 6 7 8 9) ($nxBox $nyNoz 1) simpleGrading ($RxBox 1 1)         //block 1
   hex (3 2 5 4 9 8 11 10) ($nxBox $nyBox 1) simpleGrading ($RxBox $RyBox 1)  //block 2

);

edges
(
);


boundary
(
    inlet
    {
        type patch;
        faces
        (
            (0 6 9 3)
        );
    }
    
    outlet
    {
        type patch;
        faces
        (
            (1 2 8 7)
            (2 5 11 8)
        );
    }
    
    front
    {
        type empty;
        faces
        (
            (6 7 8 9)
            (9 8 11 10)
        );
    }
    
    back
    {
        type empty;
        faces
        (
            (0 3 2 1)
            (3 4 5 2)
        );
    }
    
    chamberWall
    {
        type wall;
        faces
        (
            (4 3 9 10)
        );
    }
    
    chamberAtmoshpere //top of chamber
    {
        type patch;
        faces
        (
            (4 10 11 5)
        );
    }
    
    /*
    bottom //bottom plane
    {
        type patch;
        faces
        (
            (0 1 7 6)
        );
    } 
   */
);

mergePatchPairs
(
);

// ************************************************************************* //
