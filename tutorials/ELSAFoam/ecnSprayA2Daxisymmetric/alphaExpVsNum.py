# # Alpha experimenta vs numerical 2D test case - ECN Spray A

# Load modules
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import csv
import scipy
from scipy.interpolate import griddata
# from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import SmoothBivariateSpline
import prettyplotlib as ppl
from prettyplotlib import brewer2mpl
from scipy.integrate import simps
import os
print matplotlib.get_backend()

def lvfandPMDProfileAlongaLine(expDataFile,xRowFile,yRowFile,profileAtmm):
    
#     expDataFile = "ecnLVFaxisSym0.1to3mm.csv"
#     [xRowFile,yRowFile] = ["ecnX0.1to3.csv","ecnY0.1to3.csv"]
    alphaExp = np.loadtxt(open(expDataFile,"rb"), delimiter=",", skiprows=0)
    data = alphaExp
    xRow = np.loadtxt(open(xRowFile), delimiter=",", skiprows=0)
    yCol = np.loadtxt(open(yRowFile), delimiter=",", skiprows=0)

    sizealphaExp = alphaExp.shape
    print "alphaExp.shape[0]", alphaExp.shape[0]
    sizeX = xRow.shape
    sizeY = yCol.shape

    xlist = np.linspace(min(xRow), max(xRow), alphaExp.shape[1]) #1551
    ylist = np.linspace(max(yCol), min(yCol), alphaExp.shape[0]) #301 orig

    print "experimental data: x range %f mm to %f mm y range %f mm to %f mm " %( min(xRow), max(xRow), min(yCol), max(yCol) )
    X, Y = np.meshgrid(xlist, ylist)

    print sizealphaExp
    print sizeX
    print sizeY
    
    minval, maxval = alphaExp.min(),alphaExp.max()
    levels = np.linspace(minval,maxval,100)
    ticks=np.linspace(minval,maxval,4)

   
    print "Profile along a line from exp data"
#     profileAtmm = 0.1
    xline = profileAtmm
    yline = np.linspace(min(yCol),max(yCol), sizeY[0])
#    
    print "First interpolate the actual data"
    # construct interpolation function
    # (assuming your data is in the 2-d array "data")
    f = scipy.interpolate.interp2d(xlist,ylist,data)
    pmd = f(xline,yline)
    test = pmd - data[:,0]
    return yline, pmd
# lvfandPMDProfileAlongaLine()


def compareAlphaLinePlots():
    latestTime = np.loadtxt(open("latestTime.txt"))
    print "latestTime", latestTime
    filePathSim = os.path.abspath("postProcessing/singleGraph/"+str(latestTime))
    print "filePathSim", filePathSim
    alphaSimData = np.loadtxt(open(filePathSim+"/line_alpha.nDodecane_20.xy"), delimiter=" ", skiprows=0)
    ySim20 =  alphaSimData[:,0]
    alphaSim20 = alphaSimData[:,1]
#     change scale to mm
    ySim20 =  ySim20*1000
    print "ySim20size", ySim20.shape[0]
    print "alphaSim20size", alphaSim20.shape[0]

# load experimental data
    expDataFile = "expData/ecnLVFaxisSym0.1to3mm.csv"
    [xRowFile,yRowFile] = ["expData/ecnX0.1to3.csv","expData/ecnY0.1to3.csv"]
    [yExp,alphaExp] = lvfandPMDProfileAlongaLine(expDataFile,xRowFile,yRowFile,2) #at 2 mm
    
#    plot data
    plt.figure(figsize=(8,6))
    
    ppl.plot(yExp,alphaExp,linewidth=4, label="Exp")
    ppl.plot(ySim20, alphaSim20, '-',linewidth=4, label="elsaBaseFoam")
    plt.xlim(0,0.2)
    plt.ylim(0,)
    
    ppl.legend()
    plt.grid()
    plt.xlabel('Radial distance [mm]' , {'fontsize': 12})
    plt.ylabel('Liquid Volume Fraction [-]' , {'fontsize': 12})
    plt.xticks(size=12)
    plt.yticks(size=12)
    plt.savefig('LVFexpNum_20.pdf')
    plt.show()
    
compareAlphaLinePlots()

def compareAlphaLinePlots2():
    latestTime = np.loadtxt(open("latestTime.txt"))
    filePathSim = os.path.abspath("postProcessing/singleGraph/"+str(latestTime))
    alphaSimData = np.loadtxt(open(filePathSim+"/line_alpha.nDodecane_01.xy"), delimiter=" ", skiprows=0)
#    alphaSimData = np.loadtxt(open("line_alpha.nDodecane_01.xy"), delimiter=" ", skiprows=0)
    ySim01 =  alphaSimData[:,0]
    alphaSim01 = alphaSimData[:,1]
#     change scale to mm
    ySim01 =  ySim01*1000


    expDataFile = "expData/ecnLVFaxisSym0.1to3mm.csv"
    [xRowFile,yRowFile] = ["expData/ecnX0.1to3.csv","expData/ecnY0.1to3.csv"]
    [yExp,alphaExp] = lvfandPMDProfileAlongaLine(expDataFile,xRowFile,yRowFile,2) #at 2 mm
    [yExp,alphaExp] = lvfandPMDProfileAlongaLine(expDataFile,xRowFile,yRowFile,0.1) #at 2 mm
    
    
    plt.figure(figsize=(8,6))

    ppl.plot(yExp,alphaExp,linewidth=4, label="Exp")
    ppl.plot(ySim01, alphaSim01, '-',linewidth=4,label="elsaBaseFoam" )
    plt.xlim(0,0.1)
    plt.ylim(0,)
    
    ppl.legend()
    plt.grid()
    plt.xlabel('Radial distance [mm]' , {'fontsize': 12})
    plt.ylabel('Liquid Volume Fraction [-]' , {'fontsize': 12})
    plt.xticks(size=12)
    plt.yticks(size=12)
    plt.savefig('LVFexpNum_01.pdf')
    plt.show()

    
compareAlphaLinePlots2()


def compareAlphaLinePlots3():
    latestTime = np.loadtxt(open("latestTime.txt"))
    filePathSim = os.path.abspath("postProcessing/singleGraph/"+str(latestTime))
    alphaSimData = np.loadtxt(open(filePathSim+"/line_alpha.nDodecane_60.xy"), delimiter=" ", skiprows=0)
#    alphaSimData = np.loadtxt(open("line_alpha.nDodecane_60.xy"), delimiter=" ", skiprows=0)
    ySim60 =  alphaSimData[:,0]
    alphaSim60 = alphaSimData[:,1]
#     change scale to mm
    ySim60 =  ySim60*1000

    expDataFile = "expData/ecnLVFaxisSym3to6mm.csv"
    [xRowFile,yRowFile] = ["expData/ecnX3to6.csv","expData/ecnY3to6.csv"]
    [yExp,alphaExp] = lvfandPMDProfileAlongaLine(expDataFile,xRowFile,yRowFile, 6) #at 6 mm


    plt.figure(figsize=(8,6))
    
    ppl.plot(yExp,alphaExp,linewidth=4,label="Exp")
    ppl.plot(ySim60, alphaSim60, '-',linewidth=4,label="elsaBaseFoam")
    plt.xlim(0,0.8)
    plt.ylim(0,)

    ppl.legend()
    plt.grid()
    plt.xlabel('Radial distance [mm]' , {'fontsize': 12})
    plt.ylabel('Liquid Volume Fraction [-]' , {'fontsize': 12})
    plt.xticks(size=12)
    plt.yticks(size=12)
    plt.savefig('LVFexpNum_60.pdf')
    plt.show()
    
compareAlphaLinePlots3()

